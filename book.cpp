/******************************************************************************************************************************************
								INCLUDE (HEADER FILES)
******************************************************************************************************************************************/
#include"Book.h"


using namespace std;
vector <struct Library> bookArrange;			/**** Vector declared ****/
map <string,struct Library> bookAvailable;		/**** Mapping Done ****/
FILE *fp;

class FileUtilities					/**** Class declaration ****/
{
	private:
		vector <string> tokenize(string l, string del = "\t")	/**** tokenize with the help of vector ****/
		{
		    	vector <string> tokenizedOp;
		    	int start = 0;
		    	int end = l.find(del);
		    	tokenizedOp.push_back(l.substr(start, end - start));	/**** using push_back modifier****/ 
		    	while (end != -1) 
		    	{
				start = end + del.size();
				end = l.find(del, start);
				tokenizedOp.push_back(l.substr(start, end - start));

		    	}
	     	return tokenizedOp;
		}
		
		
	public:
		void loadFileIntoMemory()				/**** File handling ****/
		{
			int line = 0;
			char input[SIZE];
			fp = fopen("book.txt","r");
			while (fgets(input , SIZE, fp))
			{
				line ++;
				string str(input);
				vector<string> l = tokenize(str, "\t");
				struct Library book ;
				book.w = l[0];
				book.t = l[1];
				book.pri = stof(l[2]);
				book.left = stoi(l[3]);
				bookArrange.push_back(book);
			}
			fclose (fp);
			for(int inc = 0 ; inc < bookArrange.size() ; ++inc)
			{
				bookAvailable[bookArrange[inc].t] = bookArrange[inc];
			}
		}
		
		
		void persistDataIntoFile()				/**** data entered in a file and reading it successfully ****/
		{
			ofstream file;
			file.open("book.txt",ios :: out | ios::in);
			for(int inc = 0 ; inc < bookArrange.size() ; ++inc)
			{
				file << bookArrange[inc].w << "\t" << bookArrange[inc].t << "\t" << bookArrange[inc].pri << "\t" << bookArrange[inc].left << "\n";
			}
				
		}
		
};

class Book	
{
	private:
		string writer;
		string title;
		float price;
		int stock;
	
	public:
		void display();
		void writedata(FileUtilities *fp);
		bool search();
}b;

/************************************************************ FUNCTION INITIALIZATION ****************************************************/


void Book :: display()
{ 
	for(int inc = 0 ; inc < bookArrange.size() ; ++inc)
	{
		cout << bookArrange[inc].w << "\t" << bookArrange[inc].t << "\t" << bookArrange[inc].pri << "\t" << bookArrange[inc].left << "\n";
	}
	cout << "Data printed\n";
}


bool Book :: search()
{
	string str;
	cin.ignore();
	cout << "Enter the title of the book.\n";
	getline(cin,str);
	transform(str.begin(), str.end(), str.begin(), ::toupper);
	std :: map<std :: string, struct Library> :: iterator it = bookAvailable.find(str);
	if(it != bookAvailable.end())
	{
		cout << "The book is available.\n";
		return SUCCESS;
	}
	cout << "The book is unavilable.\n";
	return FAILURE;
}


void Book:: writedata (FileUtilities *fp)
{
	int len, len2;
	int count, count2;
	char writer_arr[len+1], title_arr[len2 + 1];
	struct Library b1;
	cin.ignore();
	cout<<"\nEnter Writer Name.\n";
	getline(cin, b1.w);
	transform(b1.w.begin(), b1.w.end(), b1.w.begin(), ::toupper);
	len = b1.w.length();
	strcpy(writer_arr, b1.w.c_str());
	cout<<"\nEnter the title of the book.\n";
	getline(cin, b1.t);
	transform(b1.t.begin(), b1.t.end(), b1.t.begin(), ::toupper);
	len2 = b1.t.length();
	strcpy(title_arr, b1.t.c_str());
	cout << "\nEnter the price and copies of the book: ";
	cin >> b1.pri;
	cin >> b1.left;
	for ( count = 0; count < len; count++)
	{
		if ((writer_arr[count] < 65) || (writer_arr[count] > 90))
		{
			cout << "Invalid name of writer" << endl;
			exit (0);
		}
	}
			
	for ( count2 = 0; count2 < len; count2 ++)
	{
		if ((title_arr[count2] < 65) || (title_arr[count2] > 90))
		{
			cout << "Invalid name of book" << endl;
			exit (0);
		}
	}
			
	if (((b1.pri > 0) && (b1.left > 0)) && ((title_arr[count2] > 64) || (title_arr[count2] < 91)) && ((writer_arr[count] > 64) || (writer_arr[count] < 91)))
	{
		bookArrange.push_back(b1);
		fp->persistDataIntoFile();
	}
	else 
	{
		cout << "The data cannot be entered as it is invalid data" << endl;
	}	
}


/********************************************************** MAIN FUNCTION ****************************************************************/

int main()
{
	Book b;
	int choice;
	FileUtilities *f = new FileUtilities();
	f->loadFileIntoMemory();			/**** accessing data with the help of object pointer ****/
	while (1)
	{
		cout<<"\n\n\t\tMENU" << "\n1. Display all books available in library" << "\n2. Entry of a new Book" << "\n3. Search For Book" << "\n4. Exit" << endl;
		cout<< "\n\nEnter your Choice: ";
		cin >> choice;
		if (choice ==1 || choice ==2 || choice ==3 || choice ==4 )
			{
				cout << "OK.\n";
			}
			else
			{
				cout << "Invalid choice.\n"; 
				exit (0);
			}
			
		switch(choice)	
			{
				case DISPLAY_DATA:
					b.display();
					break;
					
				case ENTER_DATA:
					b.writedata(f);
					break;
					
				case SEARCH_DATA:
					b.search();
					break;
					
				case EXIT_FINAL:
					exit (0);
					
				default: 
					cout<<"\nInvalid Choice Entered";
			}
	}
	return 0;
}
			
	
	
	
	
