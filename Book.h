/*******************************************************************************************************************************************
								INCLUDE (HEADER FILES)
*******************************************************************************************************************************************/

#include<iostream>
#include<string.h>
#include<stdlib.h>
#include<fstream>
#include<vector>
#include <cstring>
#include <bits/stdc++.h>

using namespace std;
/*******************************************************************************************************************************************
								MACROS
*******************************************************************************************************************************************/

#define SIZE 20000
#define SUCCESS 1
#define FAILURE 0

/*******************************************************************************************************************************************
								STRUCTURE DECLARATION
*******************************************************************************************************************************************/
struct Library
{
	string w;
	string t;
	float pri;
	int left;
}l;


/*******************************************************************************************************************************************
								FUNCTION DECLARATION
*******************************************************************************************************************************************/

/*** to read the data of books available in library ***/ 
	void loadFileIntoMemory();
		
/*** to help any new data to persist in file and maintain its record ***/
	void persistDataIntoFile();

/*** used for displaying the data of books available in library ***/
	void display();
		
		
/*** To see whether the respective book is available in library or not ***/ 
	bool search();
	
/*** All possible choices which can be entered by user using enum***/
	
enum choice {DISPLAY_DATA = 1, 
	     ENTER_DATA,
	     SEARCH_DATA,
	     EXIT_FINAL};






	
